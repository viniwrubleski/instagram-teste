
var observableArray = require('data/observable-array');
var images = new observableArray.ObservableArray([]);

var observableModule = require('data/observable');
var pageData = new observableModule.Observable();

var clientId = '467ede5a6b9b48ae8e03f4e2582aeeb3';
var http = require('http');

http.getJSON('https://api.instagram.com/v1/tags/nativescript/media/recent?client_id=' + clientId)
    .then(function (res) {
        res.data.forEach(function (data) {
            images.push({
                name: data.caption.text,
                thumb: data.images.thumbnail.url
            });
        });
    }, function (e) {
        console.log(e);
    });

function pageLoaded(args) {
    var page = args.object;
    pageData.set('images', images);
    page.bindingContext = pageData;
}

exports.pageLoaded = pageLoaded;
